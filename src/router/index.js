import Vue from 'vue';
import VueRouter from 'vue-router';
import CalendarKai from '../views/one-house-one-price.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'CalendarKai',
    component: CalendarKai
  }
]

const router = new VueRouter({
  // mode: 'history',
  // base: '/ipang-webview/one-house-one-price/',
  routes
})

export default router