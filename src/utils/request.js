import axios from 'axios'
import axiosRetry from 'axios-retry'
import { Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'

// create an axios instance
const service = axios.create({
    baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
    // withCredentials: true, // send cookies when cross-domain requests
    timeout: 30000 // request timeout
})

// request interceptor
service.interceptors.request.use(
    config => {
        console.log(config)
        // do something before request is sent
        if (store.state.token) {
            // let each request carry token
            // ['X-Token'] is a custom headers key
            // please modify it according to the actual situation
            config.headers['Authorization'] = 'Token ' + getToken()
        }
        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    /**
     * If you want to get http information such as headers or status
     * Please return  response => response
     */

    /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */
    response => {
        const res = response.data
            // if the custom code is not 20000, it is judged as an error.
        if (res.status !== 0) {
            Message({
                message: res.detail || 'Error',
                type: 'error',
                duration: 2 * 1000
            })

            // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
            if (res.code === 1 || res.code === 405 || res.code === 50014) {
                // to re-login
            }
            return Promise.reject(new Error(res.message || 'Error'))
        } else {
            return res
        }
    },
    error => {
        // console.log('err' + error) // for debug
        return Promise.reject(error)
    }
)

//解决丢包问题，请求超时，再次发起请求
axiosRetry(service, {
    retries: 3, // 设置自动发送请求次数
    shouldResetTimeout: true, //  重置超时时间
    retryCondition: error => {
        //true为打开自动发送请求，false为关闭自动发送请求
        if (error.message.includes('timeout') || error.message.includes("status code")){
            // Message({
            //     message: "网络请求超时，请检查是否连接网络",
            //     type: 'error',
            //     duration: 2 * 1000
            // })
            return true;
        }else{
            Message({
                message: error.message,
                type: 'error',
                duration: 2 * 1000
            })
            return false;
        }
    }
});

export default service
