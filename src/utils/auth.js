import Cookies from 'js-cookie'

const TokenKey = 'token'

export function getToken() {
  // return window.localStorage.getItem('TOKEN')
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  // return window.localStorage.setItem('TOKEN', token)
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  // return window.localStorage.removeItem('TOKEN')
  return Cookies.remove(TokenKey)
}
