import request from '@/utils/request'

//初始化jssdk
export function initWXSDK(url) {
    return request({
        url: `/mp/api/share?target_url=${url}`,
        method: 'get'
    })
}

export function uploadCalendarImg(data) {
    return request({
        url: `/public/api/upload`,
        data,
        method: 'post'
    })
}

//一房一价
export function oneHousePriceAPI(kaiId) {
    return request({
        url: `/product/admin/v3/pan/room/${kaiId}`
    })
}

//开盘信息
export function kaiPanAPI(kaiId) {
    return request({
        url: `/product/mall/v6/pan/${kaiId}`
    })
}