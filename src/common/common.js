function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
      u8arr[n] = bstr.charCodeAt(n);
   }
   return new File([u8arr], filename, {type:mime});
}

//获取hash值
function getHashValue(key) {
   // var matches = location.hash.match(new RegExp(key+'=([^&]*)'));
   // return matches ? matches[1] : null;

   var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)", "i");
   var r = window.location.search.substr(1).match(reg);
   if (r != null) return decodeURI(r[2]);
}

// 随机名称
function getUUID () {
   return `${str4()}${str4()}-${str4()}-${str4()}-${str4()}-${str4()}${str4()}${str4()}`
 }

function str4 () {
   return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
}

export { dataURLtoFile, getHashValue, getUUID }